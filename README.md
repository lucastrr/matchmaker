# Matchmaker #

This is a project to find users of the social network Twitter that have the similar writing pattern. For better sense of design visualize the design diagrams and the main features in the **diagrams** folder.

### Prerequisites
* Java 8

### Execution ###
To run you can import the folder in your favorite IDE. Or you can import the JAR file (**code.jar**). To import in the Eclipse for example, import the folder (**br**) to the folder (**src**) of your project and the other folders to the root of your project. The project can be executed in three versions (console, interface and web), as can be seen below:

* **/br/ufrn/imd/tpmmaker/ConsoleApplication.java** : To run the application on the console. Where search parameter is the user being searched without the @.;
* **/br/ufrn/imd/tpmmaker/GUIApplication.java** : To run the application on the interface. 
Where search parameter is the user being searched without the @.;
* **/br/ufrn/imd.tpmmaker/WebServer.java** :To start server JSON. Start JSON server, then access the browser and access the following link **127.0.0.1:9009/?handle=parameter**. Where parameter is the user to be sought, for example the account of CNN: **127.0.0.1:9009/?handle=cnn**

Remember the search parameter is the user being searched without the @.

### Tests 
Unit tests for the main features have been implemented, they can be accessed in the folder **br/UFRN/imd/tpmmakerTest**.

### Authors
* **Lucas Torres** - [Bitbucket](https://bitbucket.org/lucastrr/) - [Lattes](http://lattes.cnpq.br/3658283877216701)
* **Samuel Natã** - [Bitbucket](https://bitbucket.org/naelz/) - [Lattes](http://lattes.cnpq.br/8211344891596236)
* **Vítor Godeiro** - [Bitbucket](https://bitbucket.org/vitorgodeiro/) - [Lattes](http://lattes.cnpq.br/5212422823333550)