package br.ufrn.imd.facebookRecomender;

import com.restfb.types.Page;

import br.ufrn.imd.tpmmaker.persistence.UserText;

public class facebookLikes implements UserText {

	String id;
	String name;

	public facebookLikes(Page p) {
		id = p.getId();
		name = p.getName();
	}

	@Override
	public String getLang() {
		return "NA";
	}

	@Override
	public String getText() {
		return (id + " " + name);
	}

}
