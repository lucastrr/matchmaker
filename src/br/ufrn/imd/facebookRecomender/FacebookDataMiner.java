package br.ufrn.imd.facebookRecomender;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.Page;
import com.restfb.types.User;

import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.UserText;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;

public class FacebookDataMiner implements DataMiner {
	static FacebookDataMiner instance = null;
	static String authUrl;
	static WebDriver driver;
	static String accessToken;
	static String lastUsername = "";
	static long lastUpdateAcessToken = 0;
	static long duracaoDoAcessToken = 1000 * 60 * 180;

	protected FacebookDataMiner() {
	}

	public static FacebookDataMiner getInstance() {
		if (instance == null) {
			instance = new FacebookDataMiner();
			updateAppIdDomain("160311307774322", "http://example.com/");
		}
		return instance;
	}

	@Override
	public ArrayList<UserText> fetchTextData(br.ufrn.imd.tpmmaker.User user) throws UserTextNotFoundException {
		while (true) {
			if (new GregorianCalendar().getTimeInMillis() - lastUpdateAcessToken > duracaoDoAcessToken
					|| !lastUsername.equals(user.getUsername())) {
				updateAcessToken();
			}

			FacebookClient fbClient = new DefaultFacebookClient(accessToken, com.restfb.Version.LATEST);

			Connection<Page> pages = fbClient.fetchConnection("me/likes", Page.class);

			ArrayList<UserText> list = new ArrayList<UserText>();
			for (Page p : pages.getData()) {
				list.add(new facebookLikes(p));
			}
			return list;
		}
	}

	@Override
	public br.ufrn.imd.tpmmaker.User getUserByUsername(String username) {
		if (new GregorianCalendar().getTimeInMillis() - lastUpdateAcessToken > duracaoDoAcessToken
				|| !lastUsername.equals(username)) {
			updateAcessToken();
		}

		FacebookClient fbClient = new DefaultFacebookClient(accessToken, com.restfb.Version.LATEST);

		User fbuser = fbClient.fetchObject("me", User.class);

		br.ufrn.imd.tpmmaker.User user = new br.ufrn.imd.tpmmaker.User(Long.parseLong(fbuser.getId()),
				fbuser.getName());

		return user;
	}

	public static void updateAppIdDomain(String appId, String domain) {
		authUrl = "https://graph.facebook.com/oauth/authorize?type=user_agent&client_id=" + appId + "&redirect_uri="
				+ domain + "&scope=user_about_me,user_actions.books,user_actions."
				+ "fitness,user_actions.music,user_actions.news,user_actions.video,"
				+ "user_birthday,user_education_history,user_events,user_photos,user_friends,"
				+ "user_games_activity,user_hometown,user_likes,"
				+ "user_location,user_photos,user_relationship_details,user_relationships,"
				+ "user_religion_politics,user_tagged_places,user_videos,user_website,"
				+ "user_work_history,ads_management,ads_read,email,manage_pages,"
				+ "publish_actions,read_insights,read_page_mailboxes,rsvp_event";
	}

	public static void updateAcessToken() {
		updateAppIdDomain("160311307774322", "http://example.com/");
		System.setProperty("webdirver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(authUrl);

		while (true) {
			if (!driver.getCurrentUrl().contains("facebook.com")) {
				String url = driver.getCurrentUrl();
				accessToken = url.replaceAll(".*#access_token=(.+)&.*", "$1");
				GregorianCalendar c = new GregorianCalendar();
				lastUpdateAcessToken = c.getTimeInMillis();
				driver.quit();
				FacebookClient fbClient = new DefaultFacebookClient(accessToken, com.restfb.Version.LATEST);
				lastUsername = fbClient.fetchObject("me", User.class).getName();
				break;
			}
		}
	}

}
