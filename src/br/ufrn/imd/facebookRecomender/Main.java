package br.ufrn.imd.facebookRecomender;

import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.UserText;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;

public class Main {

	public static void main(String[] args) {
		FacebookDataMiner fbdm = new FacebookDataMiner();
		fbdm.getUserByUsername("Natã");

		User user = new User();
		try {
			ArrayList<UserText> list = fbdm.fetchTextData(user);
			for (UserText l : list) {
				System.out.println(l.getText());
			}
		} catch (UserTextNotFoundException e) {
			System.out.println("Deu algum erro.");
		}
	}
}
