package br.ufrn.imd.facebookRecomender;

import br.ufrn.imd.tpmmaker.FrameworkSettings;
import br.ufrn.imd.tpmmaker.GUIApplication;
import br.ufrn.imd.tpmmaker.matching.CountBasedMatch;
import br.ufrn.imd.tpmmaker.matching.Filter;
import br.ufrn.imd.tpmmaker.matching.PatternComparator;
import br.ufrn.imd.tpmmaker.matching.PersistedUsersFilter;
import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.NeverUpdatePolicy;
import br.ufrn.imd.tpmmaker.persistence.UpdatePolicy;

public class FacebookGUI {

	public static void main(String[] args) {
		DataMiner dm = FacebookDataMiner.getInstance();
		Parser parser = new FacebookParser();
		PatternComparator comparator = new CountBasedMatch();
		Filter filter = new PersistedUsersFilter();
		UpdatePolicy policy = new NeverUpdatePolicy();

		FrameworkSettings settings = new FrameworkSettings("FaceBest", dm, parser, comparator, filter, policy);
		GUIApplication.run(settings);
	}
}
