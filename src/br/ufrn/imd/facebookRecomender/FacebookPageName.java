package br.ufrn.imd.facebookRecomender;

import br.ufrn.imd.tpmmaker.pattern.Token;

public class FacebookPageName extends Token {

	String name;

	FacebookPageName(String pageName) {
		name = pageName;
	}

	@Override
	public int getHash() {
		// System.out.println("hashing...");
		String s = name.substring(0, name.indexOf(" ", 0));
		// System.out.println("has string");
		long l = Long.parseLong(s);
		// System.out.println("end hashing.");
		return (int) l;
	}

}
