package br.ufrn.imd.facebookRecomender;

import java.util.Collection;

import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.UserText;

public class FacebookParser implements Parser {

	FacebookParser() {

	}

	@Override
	public TextPattern BuildTextPattern(Collection<UserText> text) {
		// System.out.println("FacebookParser::BuildTextPattern");
		TextPattern tp = new TextPattern();
		try {
			for (UserText st : text) {
				String raw_text = st.getText();
				// System.out.println("Texto: "+raw_text);
				FacebookPageName fpn = new FacebookPageName(raw_text);
				tp.incrementTokenFrequency(fpn);
			}
			return tp;
		} catch (NullPointerException e) {
			System.out.print(e + "Classe FacebookParser : \nMetodo: BuildTextPattern\n");
			return null;
		}
	}

}
