package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertSame;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;
import br.ufrn.imd.twitterbest.Word;

public class TextPatternTest {

	@Test
	public void testIncrementToken() {
		TextPattern tp = new TextPattern();
		Token testToken = new Word("as");
		tp.incrementTokenFrequency(testToken);
		tp.incrementTokenFrequency(testToken);
		tp.incrementTokenFrequency(testToken);
		tp.incrementTokenFrequency(testToken);
		assertSame(tp.getTokenFrequency(testToken), 4);
	}

	@Test
	public void testIncrementTokenMultiple() {
		TextPattern tp = new TextPattern();
		Token testToken = new Word("as");
		tp.incrementTokenFrequency(testToken);
		tp.incrementTokenFrequency(testToken, 1);
		tp.incrementTokenFrequency(testToken, 2);
		tp.incrementTokenFrequency(testToken, 3);
		assertSame(tp.getTokenFrequency(testToken), 7);
	}

}
