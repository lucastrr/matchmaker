package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ufrn.imd.twitterbest.Hashtag;

public class HashtagTest {

	@Test
	public void testEqualsObject() {
		Hashtag valorTeste = new Hashtag("#Teste");
		assertTrue(valorTeste.equals(new Hashtag("#Teste")));
		assertFalse(valorTeste.equals(new Hashtag("#teste")));
	}

	@Test
	public void testHashCode() {
		Hashtag valorTeste = new Hashtag("Teste");
		assertEquals(valorTeste.hashCode(), "Teste".hashCode() * -1);
		assertNotEquals(valorTeste.hashCode(), "Teste");
	}

	@Test
	public void testHashtag() {
		Hashtag valorTeste = new Hashtag("#Teste");
		assertEquals(valorTeste.getWord(), "#Teste");
		assertEquals(valorTeste.getHash(), "#Teste".hashCode() * -1);
		assertNotEquals(valorTeste.getHash(), "#Teste");
		assertNotEquals(valorTeste.getHash(), "#Teste");
	}

}
