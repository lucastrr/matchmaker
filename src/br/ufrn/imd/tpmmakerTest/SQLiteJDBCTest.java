package br.ufrn.imd.tpmmakerTest;

import org.junit.Before;

import br.ufrn.imd.tpmmaker.persistence.SQLiteJDBC;

public class SQLiteJDBCTest {

	private SQLiteJDBC jdbc;

	@Before
	public void setUp() {
		this.jdbc = SQLiteJDBC.getInstance();
		this.jdbc.setTestMode(true);
		this.jdbc.resetDatabase();
	}

}
