package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.matching.MatchUser;

public class MatchUserTest {

	@Test
	public void testBuildMatchUser() {
		MatchUser status = null;
		assertNull(status);

		status = new MatchUser(0, null);
		assertNull(status.getUser());
		assertEquals(status.getTaxaMatchmaker(), 0.0, 0.001);

		status.setTaxaMatchmaker(0.004);
		assertEquals(status.getTaxaMatchmaker(), 0.004, 0.001);
	}

	@Test
	public void testMatchUserSetTaxaMatchmaker() {
		MatchUser status = null;
		assertNull(status);

		status = new MatchUser(0, null);
		status.setTaxaMatchmaker(0.004);
		assertEquals(status.getTaxaMatchmaker(), 0.004, 0.001);
	}

	@Test
	public void testMatchUserSetUser() {
		MatchUser status = null;
		status = new MatchUser(0, null);

		User user1 = new User(123456789L, "user1");

		status.setUser(user1);
		assertEquals(status.getUser(), user1);
	}

}
