package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;
import br.ufrn.imd.tpmmaker.persistence.SQLiteJDBC;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

public class PersistenceSystemTest {

	String username = "stackstatus";

	PersistenceSystem ps;

	@Before
	public void setUp() throws Exception {
		SQLiteJDBC jdbc = SQLiteJDBC.getInstance();
		jdbc.setTestMode(true);
		jdbc.resetDatabase();
		this.ps = PersistenceSystem.getInstance();
	}

	@Test
	public void testFetchTextPattern()
			throws UserNotFoundException, TwitterConnectionException, UserTextNotFoundException {

		User user = this.ps.fetchUser(this.username);

		TextPattern tp = this.ps.fetchTextPattern(user);

		assertTrue(tp.getTokenCount() > 0);
	}

	@Test
	public void testFetchUser() throws UserNotFoundException, TwitterConnectionException {

		User u1 = this.ps.fetchUser(this.username);
		User u2 = this.ps.fetchUser(this.username);

		assertNotNull(u1);
		assertNotNull(u2);
		assertEquals(u1.getId(), u2.getId());
	}

}
