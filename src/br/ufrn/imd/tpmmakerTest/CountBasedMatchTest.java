package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.matching.FrequencyBasedMatch;
import br.ufrn.imd.tpmmaker.matching.PatternComparator;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.Factory;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;
import br.ufrn.imd.tpmmaker.persistence.SQLiteJDBC;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;

public class CountBasedMatchTest {

	PersistenceSystem ps;

	@Before
	public void setUp() throws Exception {
		SQLiteJDBC jdbc = SQLiteJDBC.getInstance();
		jdbc.setTestMode(false);
		this.ps = PersistenceSystem.getInstance();
	}

	@Test
	public void testMatchingAlgorithm() throws UserTextNotFoundException {
		double match = 0;
		PatternComparator comparator = new FrequencyBasedMatch();
		User usuario = Factory.createUserDao().getUserByUsername("cnn");

		TextPattern reference_pattern = this.ps.fetchTextPattern(usuario);
		match = comparator.calculateMatch(reference_pattern, reference_pattern);

		assertEquals(match, 1, 0.0001);

	}
}
