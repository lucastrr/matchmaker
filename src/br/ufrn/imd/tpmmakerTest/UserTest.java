package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;

public class UserTest {
	@Test
	public void testId() {
		User user1 = new User(111L, "user1");
		assertEquals(user1.getId(), 111L);
	}

	@Test
	public void testName() {
		User user1 = new User(111L, "user1");
		assertEquals(user1.getUsername(), "user1");
	}

}