package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.matching.MatchUser;
import br.ufrn.imd.tpmmaker.matching.MatchmakerSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

public class MatchmakerSystemTest {

	@Test
	public void testMatchMaker() throws UserNotFoundException, TwitterConnectionException, UserTextNotFoundException {
		MatchmakerSystem ms = new MatchmakerSystem();
		Iterable<MatchUser> scores = ms.searchMatches("cnn", 0.2);
		double value = 100;
		for (MatchUser match : scores) {
			assertFalse(match.getTaxaMatchmaker() > value);
			value = match.getTaxaMatchmaker();
		}

	}

}
