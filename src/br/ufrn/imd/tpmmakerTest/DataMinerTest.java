package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;
import br.ufrn.imd.twitterbest.TwitterDataMiner;

public class DataMinerTest {

	@Test
	public void testFetchTextData()
			throws UserNotFoundException, TwitterConnectionException, UserTextNotFoundException {

		TwitterDataMiner dm = TwitterDataMiner.getInstance();
		try {
			User user1 = dm.getUserByUsername("cnn");
			User user2 = dm.getUserByUsername("globoesportecom");

			assertTrue(dm.fetchTextData(user1).size() > 0);
			assertTrue(dm.fetchTextData(user2).size() > 0);
		} catch (Exception e) {
			System.out.println(e + "Ou limite de acesso ao Twitter");
		}
	}
}