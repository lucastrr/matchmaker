package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ufrn.imd.twitterbest.Word;

public class WordTest {

	@Test
	public void testEqualsObject() {
		Word valorTeste = new Word("Teste");
		assertTrue(valorTeste.equals(new Word("Teste")));
		assertFalse(valorTeste.equals(new Word("teste")));
	}

	@Test
	public void testGetHash() {
		Word valorTeste = new Word("Teste");
		assertEquals(valorTeste.getHash(), "Teste".hashCode());
		assertNotEquals(valorTeste.getHash(), "Teste");
	}

	@Test
	public void testHashCode() {
		Word valorTeste = new Word("Teste");
		assertEquals(valorTeste.hashCode(), "Teste".hashCode());
		assertNotEquals(valorTeste.hashCode(), "Teste");
	}

	@Test
	public void testWord() {
		Word valorTeste = new Word("Teste");
		assertEquals(valorTeste.getWord(), "Teste");
		assertEquals(valorTeste.getHash(), "Teste".hashCode());
		assertNotEquals(valorTeste.getHash(), "Teste");
		assertNotEquals(valorTeste.getHash(), "Teste");
	}

}
