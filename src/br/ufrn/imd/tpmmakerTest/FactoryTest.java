package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ufrn.imd.tpmmaker.persistence.Factory;
import br.ufrn.imd.tpmmaker.persistence.UserDao;

public class FactoryTest {

	@Test
	public void factoryUserDaoInstanceTest() {
		UserDao ud = Factory.createUserDao();
		assertTrue(ud != null);
	}

}
