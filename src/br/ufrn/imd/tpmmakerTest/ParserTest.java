package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.UserText;
import br.ufrn.imd.twitterbest.Hashtag;
import br.ufrn.imd.twitterbest.TwitterParser;
import br.ufrn.imd.twitterbest.Word;

public class ParserTest {

	private Collection<UserText> status;
	private TwitterParser twitterParser;

	@Before
	public void setUp() {
		this.status = new ArrayList<>();
		UserText st1 = new UserText() {

			@Override
			public String getLang() {
				return "en";
			}

			@Override
			public String getText() {
				return "this is a message";
			}

		};

		UserText st2 = new UserText() {

			@Override
			public String getLang() {
				return "en";
			}

			@Override
			public String getText() {
				return "this is #another message";
			}

		};

		this.status.add(st1);
		this.status.add(st2);

		this.twitterParser = new TwitterParser();
	}

	@Test
	public void testBuildNullTextPattern() {
		Collection<UserText> status = null;
		assertNull(this.twitterParser.BuildTextPattern(status));
	}

	@Test
	public void testBuildTextPattern() {
		TextPattern tp = this.twitterParser.BuildTextPattern(this.status);

		assertEquals(tp.getTokenFrequency(new Word("this")), 2);
		assertEquals(tp.getTokenFrequency(new Word("is")), 2);
		assertEquals(tp.getTokenFrequency(new Word("a")), 1);
		assertEquals(tp.getTokenFrequency(new Hashtag("another")), 1);
		assertEquals(tp.getTokenFrequency(new Word("message")), 2);
		assertEquals(tp.getTokenFrequency(new Word("other")), 0);
	}

}
