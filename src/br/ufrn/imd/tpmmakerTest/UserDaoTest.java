package br.ufrn.imd.tpmmakerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.SQLiteJDBC;
import br.ufrn.imd.tpmmaker.persistence.UserDao;
import br.ufrn.imd.twitterbest.Hashtag;
import br.ufrn.imd.twitterbest.Word;

public class UserDaoTest {
	private UserDao userDao;
	private SQLiteJDBC conn;

	@Before
	public void setUp() {
		try {
			this.conn = SQLiteJDBC.getInstance();
			this.conn.setTestMode(true);
			this.conn.resetDatabase();

			this.userDao = new UserDao();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	@Test
	public void testAddUser() {

		User user1 = new User(123456789L, "user1");
		User user2 = new User(234567891L, "user2");
		User user3 = new User(345678912L, "user3");

		this.userDao.addUser(user1);
		this.userDao.addUser(user2);

		assertTrue(this.userDao.hasUser(user1));
		assertTrue(this.userDao.hasUser(user2));
		assertFalse(this.userDao.hasUser(user3));
	}

	@Test
	public void testGetUserByUsername() {
		User user0 = new User(1234L, "user0");
		this.userDao.addUser(user0);

		User other = this.userDao.getUserByUsername("user0");
		assertNotNull(other);
		assertEquals(other.getId(), user0.getId());
	}

	@Test
	public void testGetUsers() {

		User user1 = new User(1234L, "user1");
		User user2 = new User(234567891L, "user2");
		User user3 = new User(345678912L, "user3");

		this.userDao.addUser(user1);
		this.userDao.addUser(user2);
		this.userDao.addUser(user3);

		this.userDao.getUsers();
	}

	@Test
	public void testUserPattern() {

		User user = new User(1234L, "user");

		TextPattern tp = new TextPattern();
		tp.incrementTokenFrequency(new Word("as"));
		tp.incrementTokenFrequency(new Hashtag("as"));

		this.userDao.updateUserPattern(user, tp);

		tp = this.userDao.getUserPattern(user);

		assertEquals(tp.getTokenFrequency(new Word("as")), 1);
		assertEquals(tp.getTokenFrequency(new Hashtag("as")), 1);
		assertEquals(tp.getTokenFrequency(new Word("no")), 0);
	}

}
