package br.ufrn.imd.twitterbest;

import br.ufrn.imd.tpmmaker.FrameworkSettings;
import br.ufrn.imd.tpmmaker.GUIApplication;
import br.ufrn.imd.tpmmaker.matching.Filter;
import br.ufrn.imd.tpmmaker.matching.FrequencyBasedMatch;
import br.ufrn.imd.tpmmaker.matching.PatternComparator;
import br.ufrn.imd.tpmmaker.matching.PersistedUsersFilter;
import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.NeverUpdatePolicy;
import br.ufrn.imd.tpmmaker.persistence.UpdatePolicy;

public class TwitterGUIApplication {

	public static void main(String[] args) {
		DataMiner dm = TwitterDataMiner.getInstance();
		Parser parser = new TwitterParser();
		PatternComparator comparator = new FrequencyBasedMatch();
		Filter filter = new PersistedUsersFilter();
		UpdatePolicy policy = new NeverUpdatePolicy();

		FrameworkSettings settings = new FrameworkSettings("TwitterBest", dm, parser, comparator, filter, policy);
		GUIApplication.run(settings);
	}

}
