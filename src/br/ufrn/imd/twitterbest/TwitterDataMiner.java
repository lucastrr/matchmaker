/**
 ===============================================================================
 FILE...............: DataMiner.java
 COMMENTS...........: Code responsible for implementing and managing the entire
 			........: data mining part (extraction).
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

import java.io.IOException;
import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserText;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Class to get the data from Twitter.
 */
public class TwitterDataMiner implements DataMiner {
	private static TwitterDataMiner instance = null;
	private static ConfigurationBuilder cf;
	private static Twitter twitter;

	/**
	 * Return a instance of DataMiner class
	 *
	 * @return instance of DataMiner.
	 */
	public static TwitterDataMiner getInstance() {
		if (instance == null) {
			instance = new TwitterDataMiner();
			TwitterDataMiner.cf = new ConfigurationBuilder();
			TwitterDataMiner.cf.setDebugEnabled(true).setOAuthConsumerKey("JiOnFTrfJIgccX1Df6rn34kX3")
					.setOAuthConsumerSecret("1RuG7w6n0fHImikVBl6PtVOpFy1rffulGaEOUdSyjbmyygRpYq")
					.setOAuthAccessToken("472930396-3pqArhxWb9Nd2QZgY94T4z475bt4U8L9NRf6Iusr")
					.setOAuthAccessTokenSecret("rkMeIsj3SISqEL5kfY7dnClKsMoFIBFI6cmrx0asmJqs8");

			TwitterDataMiner.twitter = new TwitterFactory(cf.build()).getInstance();
		}
		return instance;
	}

	/**
	 * Default constructor
	 */
	protected TwitterDataMiner() {
	}

	/**
	 * Get tweets from one user.
	 *
	 * @param user
	 *            : name of user to get tweets.
	 * @return : list with tweets.
	 * @throws UserTextNotFoundException
	 */
	public ArrayList<UserText> fetchTextData(User user) throws UserTextNotFoundException {
		int currrentPagen = 1;
		@SuppressWarnings({ "unchecked", "rawtypes" })
		ArrayList<UserText> statusList = new ArrayList();
		while (true) { // Enquanto conseguir pegar os tweets, continua
			try {

				int size = statusList.size();
				Paging page = new Paging(currrentPagen++, 100);
				for (Status st : twitter.getUserTimeline(user.getId(), page)) {
					statusList.add(new StatusAdaptor(st));
				}

				if (statusList.size() == size) {
					break;
				}
			} catch (TwitterException e) {
				if (statusList.isEmpty()) {
					throw new UserTextNotFoundException();
				}
				break;
			}
		}
		return statusList;
	}

	/**
	 * Get the Id from one user name.
	 *
	 * @param user
	 *            String containing the user name.
	 * @return Return the Id from the user.
	 * @throws TwitterConnectionException
	 */
	private long getId(String user) throws UserNotFoundException, TwitterConnectionException {
		Paging page = new Paging(1, 1);
		long id = 0;
		try {
			if (twitter.getUserTimeline(user, page).size() <= 0) {
				throw new IOException();
			}
			Status status = twitter.getUserTimeline(user, page).get(0);
			id = status.getUser().getId();
		} catch (TwitterException e) {
			throw new UserNotFoundException();
		} catch (IOException e) {
			throw new UserNotFoundException();
		}

		return id;
	}

	/**
	 * Get an Twitter user by his username.
	 * 
	 * @param username
	 *            : Name from the user.
	 * @return the user.
	 * @throws UserNotFoundException
	 * @throws TwitterConnectionException
	 */
	public User getUserByUsername(String username) throws UserNotFoundException {
		try {
			long id = this.getId(username);
			return new User(id, username);
		} catch (Exception e) {
			throw new UserNotFoundException(username);
		}
	}
}
