/**
 ===============================================================================
 FILE...............: Parser.java
 COMMENTS...........: Code responsible for implementing and managing all part
 			........: of the application responsible for carrying out the
 			........: treatment of the text.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

import java.util.Collection;

import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;
import br.ufrn.imd.tpmmaker.persistence.UserText;

/**
 * Class to parse and tokenize tweets.
 */
public class TwitterParser implements Parser {

	/**
	 * Default constructor
	 */
	public TwitterParser() {
	}

	/**
	 * construct a textPattern form a list of tweets
	 * 
	 * @param text
	 *            : collection of tweets
	 * @return TextPattern : pattern from the tweets
	 */
	public TextPattern BuildTextPattern(Collection<UserText> text) {

		TextPattern tp = new TextPattern();
		try {
			for (UserText st : text) {
				String raw_text = st.getText();
				String[] entities = raw_text.split("[\\s,.:;!\\?\\+\\(\\)\\*\"']");
				for (String entity : entities) {

					Token token = null;
					if (entity.isEmpty()) {
						continue;
					} else if (entity.charAt(0) == '#') {
						token = new Hashtag(entity.substring(1));
					} /*
						 * else { token = new Word(entity); }
						 */

					if (token != null) {
						tp.incrementTokenFrequency(token);
					}
				}
			}
			return tp;
		} catch (NullPointerException e) {
			// System.out.print(e+ "Classe Parser : Metodo:
			// BuildTextPattern\n");
			return null;
		}
	}

}