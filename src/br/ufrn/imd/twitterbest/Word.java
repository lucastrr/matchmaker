/**
 ===============================================================================
 FILE...............: Word.java
 COMMENTS...........: Code responsible for implementing and managing all of the
  			........: texts referring to extracted words.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

import br.ufrn.imd.tpmmaker.pattern.Token;

/**
 * Generic word token
 */
public class Word extends Token {

	private String word;

	private int hashcode;

	/**
	 * Constructor
	 */
	public Word(String word) {
		this.word = word;
		this.hashcode = word.hashCode();
	}

	/**
	 * @return integer hashcode for word
	 */
	@Override
	public int getHash() {
		return this.hashcode;
	}

	/**
	 * @return current word.
	 */
	public String getWord() {
		return this.word;
	}

	@Override
	public int hashCode() {
		return this.getHash();
	}

	@Override
	public String toString() {

		return "Word[" + this.word + "]";
	}

}