/**
 ===============================================================================
 FILE...............: TwitterConnectionException.java
 COMMENTS...........: Code responsible for implementing exception.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

/**
 * Represent the error to connect to Twitter.
 *
 */
public class TwitterConnectionException extends Exception {

	private static final long serialVersionUID = -5135261745844882724L;

	public TwitterConnectionException() {
		super("Error connecting with twitter");
	}
}
