/**
 ===============================================================================
 FILE...............: Hashtag.java
 COMMENTS...........: Code responsible for implementing and managing all of the
  			........: texts referring to #
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

import br.ufrn.imd.tpmmaker.pattern.Token;

/**
 * Represents a twitter hashtag token
 */
public class Hashtag extends Token {

	private String word;
	private int hashcode;

	/**
	 * Constructor
	 * 
	 * @param Hashtag
	 *            : string, without the '#'
	 */
	public Hashtag(String word) {
		this.word = word;
		this.hashcode = (word.hashCode() * -1);
	}

	/**
	 * @return integer hashcode for hashtag token
	 */
	@Override
	public int getHash() {
		return this.hashcode;
	}

	/**
	 * @return word from the hashtag.
	 */
	public String getWord() {
		return this.word;
	}

	@Override
	public int hashCode() {
		return this.getHash();
	}

	@Override
	public String toString() {
		return "Hashtag[#" + this.word + "]";
	}
}