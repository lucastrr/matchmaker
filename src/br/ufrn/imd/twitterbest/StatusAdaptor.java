/**
 ===============================================================================
 FILE...............: StatusAdaptor.java
 COMMENTS...........: Adaptor for Status Twitter.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.twitterbest;

import br.ufrn.imd.tpmmaker.persistence.UserText;
import twitter4j.Status;

/**
 * Adapter to the Status class from Twitter4j API. Adaptor.
 */
public class StatusAdaptor implements UserText {

	private Status status;

	/**
	 * Default creator.
	 * 
	 * @param status
	 *            : status from Twitter4j API.
	 */
	public StatusAdaptor(Status status) {
		this.status = status;
	}

	@Override
	public String getLang() {
		return this.status.getLang();
	}

	@Override
	public String getText() {
		return this.status.getText();
	}

}
