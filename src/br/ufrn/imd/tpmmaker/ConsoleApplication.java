/**
 ===============================================================================
 FILE...............: ConsoleApplication.java
 COMMENTS...........: Code responsible for implementing console application.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker;

import java.util.Locale;
import java.util.Scanner;

import br.ufrn.imd.tpmmaker.matching.MatchUser;
import br.ufrn.imd.tpmmaker.matching.MatchmakerSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

/**
 * Run the application from the terminal.
 */
public class ConsoleApplication {

	public static void main(String[] args) {

		String username;
		if (args.length == 1) {
			username = args[0];
			System.out.println("user: @" + args[0]);
		} else {
			Scanner scanner = new Scanner(System.in);
			System.out.println("choose user: ");
			username = scanner.next();
			scanner.close();
		}

		String format = "@%s\n  score:%f\n";

		MatchmakerSystem ms = new MatchmakerSystem();

		Iterable<MatchUser> scores = null;
		try {
			scores = ms.searchMatches(username, 0.2);
		} catch (UserNotFoundException e) {
			System.err.println("user not found");
			System.exit(1);
		} catch (TwitterConnectionException e) {
			System.err.println("twitter connection error");
			System.exit(1);
		} catch (UserTextNotFoundException e) {
			System.err.println("text not found for user");
			System.exit(1);
		}

		for (MatchUser match : scores) {
			System.out.print(
					String.format(Locale.ENGLISH, format, match.getUser().getUsername(), match.getTaxaMatchmaker()));
		}
	}
}