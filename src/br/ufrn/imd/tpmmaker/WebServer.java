/**
 ===============================================================================
 FILE...............: WebServer.java
 COMMENTS...........: Code responsible for WebServer Json for application.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker;

import br.ufrn.imd.tpmmaker.server.JSONServer;

/**
 * Represent a webServer.
 */
public class WebServer {

	public static void run(FrameworkSettings settings) {
		FrameworkSettings.setGlobalSettings(settings);
		JSONServer.run();
	}
}
