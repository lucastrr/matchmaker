/**
 ===============================================================================
 FILE...............: User.java
 COMMENTS...........: Code responsible for implementing and represent the
         ...........: application users.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker;

import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.UserText;

/**
 * Represent an Twitter User
 */
public class User {
	long id;
	ArrayList<UserText> tweetList;
	private String username;

	/**
	 * Default constructor
	 */
	public User() {
		this.username = null;
		this.tweetList = null;
		this.id = -1;
	}

	/**
	 * Constructor with parameters.
	 * 
	 * @param id
	 *            : Id from the user.
	 * @param username
	 *            : name from the user.
	 */
	public User(long id, String username) {
		this.id = id;
		this.username = username;
		this.tweetList = null;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof User)) {
			return false;
		}
		return (this.getId() == ((User) obj).getId());
	}

	/**
	 * Get the id from the current user.
	 *
	 * @return user id.
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Get the user name from the user.
	 *
	 * @return user name.
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Print the Tweets on terminal.
	 * 
	 * @param n
	 */
	public void printTweets(int n) {
		for (int i = 0; i < n; i++) {
			System.out.println((i + 1) + " - " + this.tweetList.get(i));
		}
		System.out.println("\n\n");
	}

	/**
	 * Update the user Id.
	 * 
	 * @param newId
	 *            : new id.
	 */
	public void setId(long newId) {
		this.id = newId;
	}

	/**
	 * Change the user write pattern.
	 * 
	 * @param pattern
	 *            : new pattern.
	 */
	public void setPattern(TextPattern pattern) {
	}
}
