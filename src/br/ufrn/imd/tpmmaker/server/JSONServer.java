/**
 ===============================================================================
 FILE...............: JSONServer.java
 COMMENTS...........: Code responsible for implementing Server Json.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.server;

import java.util.Locale;

import br.ufrn.imd.tpmmaker.matching.CountBasedMatch;
import br.ufrn.imd.tpmmaker.matching.MatchUser;
import br.ufrn.imd.tpmmaker.matching.MatchmakerSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import fi.iki.elonen.util.ServerRunner;

/**
 * Represent a JSON server.
 */
public class JSONServer extends NanoHTTPD {

	private static final String MIME_TYPE = "application/json";

	/**
	 * run the JSON server.
	 */
	public static void run() {
		ServerRunner.run(JSONServer.class);
	}

	/**
	 * Default creator.
	 */
	public JSONServer() {
		super(9009);
	}

	@Override
	public Response serve(IHTTPSession session) {

		String handle = session.getParms().get("handle");
		StringBuilder sb = new StringBuilder();

		System.out.println("searching for @" + handle);

		MatchmakerSystem ms = new MatchmakerSystem();
		ms.setPatternComparator(new CountBasedMatch());
		Iterable<MatchUser> scores;
		try {
			scores = ms.searchMatches(handle, 0.0);
		} catch (UserNotFoundException | TwitterConnectionException | UserTextNotFoundException e) {

			return newFixedLengthResponse(Status.INTERNAL_ERROR, MIME_TYPE, "{ \"error\": \"" + e.toString() + "\" }");
		}

		String format = "%s{ \"handle\": \"@%s\", \"match\" : %f }";

		boolean first = true;
		String sep = "\n\t";

		sb.append("{ \"scores\":[");
		for (MatchUser m : scores) {
			System.out.println("writing " + m.getUser().getUsername());
			sb.append(String.format(Locale.ENGLISH, format, sep, m.getUser().getUsername(), m.getTaxaMatchmaker()));
			if (first) {
				sep = ",\n\t";
				first = false;
			}
		}
		sb.append("\n]}");

		return newFixedLengthResponse(Status.OK, MIME_TYPE, sb.toString());
	}
}
