/**
 ===============================================================================
 FILE...............: UserNotFoundException.java
 COMMENTS...........: Code responsible for implementing exception.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

/**
 * Represent the error caused by can't find a user in Twitter.
 */
public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = 4955573295542902294L;

	public UserNotFoundException() {
		super("User not found");
	}

	public UserNotFoundException(String username) {
		super("User @" + username + " not found.");
	}
}
