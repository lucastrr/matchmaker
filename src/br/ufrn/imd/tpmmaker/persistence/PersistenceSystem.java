/**
 ===============================================================================
 FILE...............: PersistenceSystem.java
 COMMENTS...........: System of the persistence
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import java.util.Collection;

import br.ufrn.imd.tpmmaker.FrameworkSettings;
import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

/**
 * Coordinates database and web data retrieval. Facade and Singleton
 */
public class PersistenceSystem {

	private static PersistenceSystem instance;

	/**
	 * @return singleton instance of PersistenceSystem
	 */
	public static PersistenceSystem getInstance() {
		if (instance == null) {
			instance = new PersistenceSystem();
		}
		return instance;
	}

	private UserDaoInterface userDao;
	private Parser parser;
	private DataMiner miner;

	/**
	 * Default creator.
	 */
	private PersistenceSystem() {
		this.userDao = Factory.createUserDao();
		this.parser = FrameworkSettings.getGlobalSettings().getParser();
		this.miner = FrameworkSettings.getGlobalSettings().getDataMiner();
	}

	/**
	 * Obtains the text pattern for a user
	 *
	 * If information not found in database, search on web and store result.
	 *
	 * @param user
	 * @return user's text pattern
	 * @throws UserTextNotFoundException
	 */
	public TextPattern fetchTextPattern(User user) throws UserTextNotFoundException {

		TextPattern tp = this.userDao.getUserPattern(user);

		if (tp.getTokenCount() == 0) { // if nothing on database, fetch from
										// twitter

			// System.out.println("DEBUG -------");
			Collection<UserText> status = miner.fetchTextData(user);
			/*
			 * for( UserText t : status ){ System.out.println("Id Nome :: " +
			 * t.getText()); } System.out.println("END DEBUG ---");
			 */
			tp = this.parser.BuildTextPattern(status);
			this.userDao.updateUserPattern(user, tp);

		}

		return tp;
	}

	/**
	 * Obtains information for a given user, by username.
	 *
	 * First search for data in database. If not found, fetch info from web and
	 * stores result.
	 *
	 * @param username
	 * @return
	 * @throws UserNotFoundException,
	 *             TwitterConnectionException
	 */
	public User fetchUser(String username) throws UserNotFoundException, TwitterConnectionException {
		System.out.println("Geting user: " + username);
		User user = this.userDao.getUserByUsername(username);
		if (user == null) {
			user = miner.getUserByUsername(username);
			username = user.getUsername();
			this.userDao.addUser(user);
		}

		return user;
	}

	/**
	 * @return List of users stored in database
	 */
	public Iterable<User> fetchUserList() {
		return this.userDao.getUsers();
	}
}
