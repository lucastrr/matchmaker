/**
 ===============================================================================
 FILE...............: Factory.java
 COMMENTS...........: Factory
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

/**
 * Implements the Factory project pattern.
 */
public class Factory {

	public static UserDao createUserDao() {
		return new UserDao();
	}
}
