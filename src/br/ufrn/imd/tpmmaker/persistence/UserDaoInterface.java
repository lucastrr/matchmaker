/**
 ===============================================================================
 FILE...............: DataMiner.java
 COMMENTS...........: Code responsible for declaration dao User.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;

public interface UserDaoInterface {

	/**
	 * Adds user to database
	 *
	 * @param user
	 */
	public void addUser(User user);

	/**
	 * Search for user info in database.
	 *
	 * @param username
	 *            username for the twitter account
	 * @return user info, or null if not found.
	 */
	public User getUserByUsername(String username);

	/**
	 * Gets text pattern for user.
	 *
	 * @param user
	 * @return text pattern
	 */
	public TextPattern getUserPattern(User user);

	public Iterable<User> getUsers();

	public boolean hasUser(User user);

	/**
	 * Updates the text pattern stored for a user
	 *
	 * @param user
	 * @param text
	 *            pattern
	 */
	public void updateUserPattern(User user, TextPattern tp);

}
