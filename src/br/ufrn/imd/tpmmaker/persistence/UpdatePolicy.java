package br.ufrn.imd.tpmmaker.persistence;

import java.util.Date;

public interface UpdatePolicy {
	/**
	 * Return the next update time given the last one. Return null to not
	 * update;
	 */
	public Date nextUpdate(Date last_update);
}
