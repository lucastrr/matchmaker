/**
 ===============================================================================
 FILE...............: SQLiteJDBCConection.java
 COMMENTS...........: Code responsible for implementing and managing the JDBC
 			........: connection.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.ufrn.imd.tpmmaker.FrameworkSettings;

/**
 * Class to make the connection to the SQLite DB
 *
 */
public abstract class SQLiteJDBCConection implements DataBase {

	boolean test_mode;

	public SQLiteJDBCConection() {
		this.test_mode = false;
	}

	@Override
	public void closeConnection(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	@Override
	public Connection makeConnection() throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		String uri = "jdbc:sqlite:";
		if (this.test_mode) {
			uri += "test.db";
		} else {
			uri += FrameworkSettings.getGlobalSettings().getDBName() + ".db";
		}

		Connection conn = DriverManager.getConnection(uri);
		return conn;
	}

	/**
	 * Use to enter/exit the test mode. In test mode, a different database is
	 * used.
	 *
	 * @param test
	 *            true if entering test mode, false otherwise
	 */
	public void setTestMode(boolean test) {
		try {
			this.test_mode = test;
		} catch (Exception e) {
			System.out.println("SQLiteJDBCConnect error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	public abstract void start();

}
