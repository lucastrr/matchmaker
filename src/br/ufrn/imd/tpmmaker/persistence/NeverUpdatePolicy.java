package br.ufrn.imd.tpmmaker.persistence;

import java.util.Date;

public class NeverUpdatePolicy implements UpdatePolicy {

	@Override
	public Date nextUpdate(Date last_update) {
		return null;
	}

}
