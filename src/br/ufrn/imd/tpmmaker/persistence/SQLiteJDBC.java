/**
 ===============================================================================
 FILE...............: SQLiteJDBC.java
 COMMENTS...........: Code responsible for implementing and managing the JDBC
 			........: table.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import java.sql.Connection;
import java.sql.Statement;

/**
 * Class to do a connection to the SQLite DB. Singleton.
 */
public class SQLiteJDBC extends SQLiteJDBCConection {

	private static SQLiteJDBC instance;

	/**
	 * @return a instance from the class.
	 */
	public static SQLiteJDBC getInstance() {
		if (instance == null) {
			instance = new SQLiteJDBC();
		}
		return instance;
	}

	/**
	 * Clear all Database.
	 */
	private void clearDatabase() {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = this.makeConnection();
			stmt = conn.createStatement();

			String sql = "DROP TABLE User";
			stmt.executeUpdate(sql);

			sql = "DROP TABLE TokenFrequency";
			stmt.executeUpdate(sql);

			stmt.close();
			this.closeConnection(conn);
		} catch (Exception e) {
			System.out.println("SQLiteJDBC::ClearConnection error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Generate the tables on the Database.
	 */
	private void generateTables() {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = this.makeConnection();

			stmt = conn.createStatement();

			String sql = "CREATE TABLE IF NOT EXISTS User" + "(id       INT   NOT NULL," + " username TEXT  NOT NULL,"
					+ " PRIMARY KEY (id))";

			stmt.executeUpdate(sql);

			sql = "CREATE TABLE IF NOT EXISTS TokenFrequency" + "(user_id     INT   NOT NULL,"
					+ " token       INT   NOT NULL," + " frequency   INT   NOT NULL,"
					+ " FOREIGN KEY (user_id) REFERENCES User(id)," + " PRIMARY KEY (user_id, token))";

			stmt.executeUpdate(sql);

			stmt.close();
			this.closeConnection(conn);
		} catch (Exception e) {
			System.out.println("SQLiteJDBC::GenerateTables error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Restarts the database
	 */
	public void resetDatabase() {
		this.clearDatabase();
		this.generateTables();
	}

	/**
	 * Run any necessary initialization routine
	 */
	public void start() {
		this.generateTables();
	}
}
