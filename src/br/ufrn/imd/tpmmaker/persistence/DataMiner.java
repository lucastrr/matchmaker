package br.ufrn.imd.tpmmaker.persistence;

import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.User;

public interface DataMiner {
	public ArrayList<UserText> fetchTextData(User user) throws UserTextNotFoundException;

	public User getUserByUsername(String username) throws UserNotFoundException;
}
