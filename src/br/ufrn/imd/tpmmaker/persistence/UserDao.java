/**
 ===============================================================================
 FILE...............: UserDao.java
 COMMENTS...........: Code responsible for implementing Dao User.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.ReferenceToken;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;

/**
 * DAO to get users form the DataBase.
 */

public class UserDao implements UserDaoInterface {

	private DataBase instance;

	/**
	 * Default creator.
	 */
	public UserDao() {
		this.instance = SQLiteJDBC.getInstance();
		instance.start();
	}

	@Override
	public void addUser(User user) {

		Connection conn = null;
		Statement stmt = null;
		try {

			conn = this.instance.makeConnection();

			stmt = conn.createStatement();

			System.out.println(Long.toString(user.getId()));
			String sql = "INSERT INTO User(id, username) VALUES (" + Long.toString(user.getId()) + "," + "'"
					+ user.getUsername() + "')";

			stmt.executeUpdate(sql);

			stmt.close();
			this.instance.closeConnection(conn);
		} catch (Exception e) {
			System.out.println("UserDao::addUser error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

	}

	@Override
	public User getUserByUsername(String username) {

		User user = null;
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = this.instance.makeConnection();

			stmt = conn.createStatement();
			String sql = "SELECT id FROM User WHERE username = '" + username + "'";

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				long id = rs.getLong("id");
				user = new User(id, username);
			}

			stmt.close();
			this.instance.closeConnection(conn);
		} catch (Exception e) {
			System.out.println("UserDao::getUserByUsername error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return user;
	}

	@Override
	public TextPattern getUserPattern(User user) {

		long user_id = user.getId();

		TextPattern tp = new TextPattern();

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.instance.makeConnection();

			stmt = conn.createStatement();
			String sql = String.format(
					"SELECT token, frequency FROM TokenFrequency " + "WHERE user_id = " + Long.toString(user_id));

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int token_hash = rs.getInt(1);
				int count = rs.getInt(2);

				tp.incrementTokenFrequency(new ReferenceToken(token_hash), count);
			}

			stmt.close();
			this.instance.closeConnection(conn);

			return tp;
		} catch (Exception e) {
			System.out.println("UserDao::getUserPattern error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return null;
	}

	@Override
	public Iterable<User> getUsers() {

		ArrayList<User> users = new ArrayList<>();

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.instance.makeConnection();
			stmt = conn.createStatement();

			String sql = String.format("SELECT id, username FROM User");

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				long id = rs.getLong(1);
				String username = rs.getString(2);
				users.add(new User(id, username));
				// System.out.println("Id: " + id + " name: " + username);
			}

			stmt.close();
			this.instance.closeConnection(conn);
			// System.out.println("quantidade: " + users.size());

			return users;
		} catch (Exception e) {
			System.out.println("UserDao::getUsers error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return null;
	}

	@Override
	public boolean hasUser(User user) {

		boolean found = false;
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.instance.makeConnection();
			stmt = conn.createStatement();

			String sql = String.format("SELECT count(id) FROM User WHERE id = " + Long.toString(user.getId()));

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				int count = rs.getInt(1);
				found = (count > 0);
			}

			stmt.close();
			this.instance.closeConnection(conn);
		} catch (Exception e) {
			System.out.println("UserDao::hasUser error >>> ");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return found;
	}

	@Override
	public void updateUserPattern(User user, TextPattern tp) {

		long user_id = user.getId();

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.instance.makeConnection();
			conn.setAutoCommit(false);
			stmt = conn.createStatement();

			String sql = String.format("DELETE FROM TokenFrequency " + "WHERE user_id = " + Long.toString(user_id));

			stmt.executeUpdate(sql);

			for (Token tk : tp.getTokens()) {
				int hash = tk.getHash();
				int count = tp.getTokenFrequency(tk);

				sql = "INSERT INTO TokenFrequency (user_id, token, frequency)" + " VALUES (" + Long.toString(user_id)
						+ "," + Integer.toString(hash) + "," + Integer.toString(count) + ")";

				stmt.executeUpdate(sql);
			}

			conn.commit();
			stmt.close();
			this.instance.closeConnection(conn);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}
}
