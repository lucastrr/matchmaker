/**
 ===============================================================================
 FILE...............: DataBase.java
 COMMENTS...........: Interface that represents the connection to the database
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

import java.sql.Connection;
import java.sql.SQLException;

public interface DataBase {

	/**
	 * Run any necessary initialization
	 */
	public void start();

	/**
	 *
	 * @param conn
	 *            : connection
	 */
	public void closeConnection(Connection conn);

	/**
	 *
	 * @return Connection
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection makeConnection() throws SQLException, ClassNotFoundException;
}
