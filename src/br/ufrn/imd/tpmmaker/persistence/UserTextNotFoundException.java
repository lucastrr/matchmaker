/**
 ===============================================================================
 FILE...............: UserTextNotFoundException.java
 COMMENTS...........: Code responsible for implementing exception.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.persistence;

public class UserTextNotFoundException extends Exception {

	private static final long serialVersionUID = 4765595317266355232L;

	public UserTextNotFoundException() {
		super("Could not fetch user tweets.");
	}

}
