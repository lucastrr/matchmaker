package br.ufrn.imd.tpmmaker.matching;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;

public class PersistedUsersFilter implements Filter {

	@Override
	public Iterable<User> getRelevantUsers(PersistenceSystem ps) {
		return ps.fetchUserList();
	}

}
