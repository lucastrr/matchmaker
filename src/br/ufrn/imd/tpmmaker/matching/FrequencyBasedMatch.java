/**
 ===============================================================================
 FILE...............: FrequencyBasedMatch.java
 COMMENTS...........: Code responsible for implementing matching.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.matching;

import java.util.Set;
import java.util.TreeSet;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;

/**
 * Class to encapsulate an algorithm to get the frequency match from a text
 * pattern.
 */
public class FrequencyBasedMatch implements PatternComparator {

	@Override
	public double calculateMatch(TextPattern base, TextPattern reference) {

		double base_mean = base.getTotalFrequency() / (double) base.getTokenCount();
		double reference_mean = reference.getTotalFrequency() / (double) reference.getTokenCount();

		Set<Token> base_tokens = base.getTokenSet();
		Set<Token> reference_tokens = reference.getTokenSet();
		Set<Token> tokens = new TreeSet<>();
		tokens.addAll(base_tokens);
		tokens.addAll(reference_tokens);

		double acc = 0;
		for (Token tk : tokens) {
			acc += (base.getTokenFrequency(tk) - base_mean) * (reference.getTokenFrequency(tk) - reference_mean);
		}
		return acc / (base.getFrequencySD() * reference.getFrequencySD());
	}

}
