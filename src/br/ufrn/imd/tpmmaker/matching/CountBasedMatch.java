/**
 ===============================================================================
 FILE...............: CounBasedMatch.java
 COMMENTS...........: Code responsible for implementing matching the counter
         ...........: method.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.matching;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;

/**
 * Class to encapsulate an algorithm to get the count match from a text pattern.
 */
public class CountBasedMatch implements PatternComparator {

	@Override
	public double calculateMatch(TextPattern base, TextPattern reference) {

		int match_count = 0;
		for (Token tk : reference.getTokens()) {
			match_count += Math.min(base.getTokenFrequency(tk), reference.getTokenFrequency(tk));
		}
		return (match_count / (double) base.getTotalFrequency());
	}

}
