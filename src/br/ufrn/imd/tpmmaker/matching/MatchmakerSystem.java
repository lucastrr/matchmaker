/**
 ===============================================================================
 FILE...............: MatchmakerSystem.java
 COMMENTS...........: Code responsible for implementing matchmaker system.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.matching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import br.ufrn.imd.tpmmaker.FrameworkSettings;
import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

/**
 * Represent a system that make the match between text patterns.
 */
public class MatchmakerSystem {
	private double acceptThreshold;
	private User user;
	private TextPattern user_pattern;
	private PatternComparator comparator;

	/**
	 * Default creator.
	 */
	public MatchmakerSystem() {
		this.acceptThreshold = 0.0;
		this.comparator = FrameworkSettings.getGlobalSettings().getComparator();
	}

	/**
	 * 
	 * @return List<MatchUser> : Match with each user
	 * @throws UserTextNotFoundException
	 */
	private List<MatchUser> getMatching() throws UserTextNotFoundException {
		try {

			PersistenceSystem ps = PersistenceSystem.getInstance();
			Filter filter = FrameworkSettings.getGlobalSettings().getFilter();
			Iterator<User> it = filter.getRelevantUsers(ps).iterator();
			List<MatchUser> answer = new ArrayList<>();

			while (it.hasNext()) {
				User reference_user = it.next();
				if (reference_user.equals(this.user)) {
					continue;
				}

				TextPattern reference_pattern = ps.fetchTextPattern(reference_user);
				double match = this.comparator.calculateMatch(this.user_pattern, reference_pattern);

				if (match >= this.acceptThreshold) {
					// System.out.println("Adding to answer");
					MatchUser match_user = new MatchUser(match, reference_user);
					answer.add(match_user);
				} else {
					// System.out.println("not adding, match is: " + match);
				}
			}

			Collections.sort(answer);
			// System.out.println("Tamanho Da lista: " + answer.size());
			return answer;

		} catch (NullPointerException e) {
			return null;
		}
	}

	/**
	 * search matching from the parameter
	 *
	 * @param username
	 *            : Name from the user
	 * @param acceptThreshold
	 * @return Iterable<MatchUser> : Match with each user
	 * @throws UserNotFoundException
	 * @throws TwitterConnectionException
	 * @throws UserTextNotFoundException
	 */
	public Iterable<MatchUser> searchMatches(String username, double acceptThreshold)
			throws UserNotFoundException, TwitterConnectionException, UserTextNotFoundException {

		PersistenceSystem ps = PersistenceSystem.getInstance();
		this.user = ps.fetchUser(username);
		this.user_pattern = ps.fetchTextPattern(this.user);
		this.acceptThreshold = acceptThreshold;
		return this.getMatching();
	}

	/**
	 * Change the Pattern comparator.
	 * 
	 * @param comparator
	 *            : new comparator.
	 */
	public void setPatternComparator(PatternComparator comparator) {
		this.comparator = comparator;
	}
}
