package br.ufrn.imd.tpmmaker.matching;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;

public interface Filter {
	public Iterable<User> getRelevantUsers(PersistenceSystem ps);
}
