/**
 ===============================================================================
 FILE...............: MatchUser.java
 COMMENTS...........: Code responsible for represent the matchmaker.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.matching;

import br.ufrn.imd.tpmmaker.User;

/**
 * 
 *
 */
public class MatchUser implements Comparable<MatchUser> {
	private double taxaMatchmaker;
	private User user;

	/**
	 * Default creator.
	 * 
	 * @param taxaMatchmaker
	 *            :
	 * @param user
	 *            :
	 */
	public MatchUser(double taxaMatchmaker, User user) {
		this.user = user;
		this.taxaMatchmaker = taxaMatchmaker;
	}

	@Override
	public int compareTo(MatchUser o) {
		if (this.getTaxaMatchmaker() == o.getTaxaMatchmaker()) {
			return 0;
		} else if (this.getTaxaMatchmaker() < o.getTaxaMatchmaker()) {
			return 1;
		} else {
			return -1;
		}
	}

	public double getTaxaMatchmaker() {
		return this.taxaMatchmaker;
	}

	public User getUser() {
		return this.user;
	}

	public void setTaxaMatchmaker(double taxaMatchmaker) {
		this.taxaMatchmaker = taxaMatchmaker;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		String s = String.format("User: @%s, Matching: %.2f%%", this.user.getUsername(), this.taxaMatchmaker);
		return s;
	}
}
