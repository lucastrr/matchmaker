/**
 ===============================================================================
 FILE...............: PatternComparator.java
 COMMENTS...........: Code responsible for represent the pattern comparator.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.matching;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;

public interface PatternComparator {

	public double calculateMatch(TextPattern base, TextPattern reference);

}
