/**
 ===============================================================================
 FILE...............: DataRetrieval.java
 COMMENTS...........: Code responsible for implementing data retrieval.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker;

import java.util.Scanner;

import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.persistence.PersistenceSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

public class DataRetrieval {

	public static void main(String[] args) {
		DataRetrieval dr = new DataRetrieval();
		dr.run();
	}

	@SuppressWarnings("resource")
	public void run() {

		Scanner scanner = new Scanner(System.in);

		System.out.print("enter the user handle: ");
		String username = scanner.nextLine();

		PersistenceSystem ps = PersistenceSystem.getInstance();
		User user;
		try {
			user = ps.fetchUser(username);
			TextPattern tp = ps.fetchTextPattern(user);
			if (tp.getTokenCount() == 0) {
				throw new UserTextNotFoundException();
			}

		} catch (TwitterConnectionException e) {
			System.err.println("connection error");
			scanner.close();
			System.exit(1);
		} catch (UserNotFoundException e) {
			System.err.println(String.format("invalid user @%s", username));
			scanner.close();
			System.exit(1);
		} catch (UserTextNotFoundException e) {
			System.err.println(String.format("could not fetch text for @%s", username));
			scanner.close();
			System.exit(1);
		}
		scanner.close();
		System.out.println("Operation succesfull");
	}

}
