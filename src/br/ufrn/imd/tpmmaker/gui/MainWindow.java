/**
 ===============================================================================
 FILE...............: MainWindow.java
 COMMENTS...........: Code responsible for implementing GUI application.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import br.ufrn.imd.tpmmaker.FrameworkSettings;
import br.ufrn.imd.tpmmaker.matching.MatchUser;
import br.ufrn.imd.tpmmaker.matching.MatchmakerSystem;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import br.ufrn.imd.twitterbest.TwitterConnectionException;

/**
 * Visual Interface to use the application.
 */
public class MainWindow implements Runnable {

	private class MatchmakerWorker extends SwingWorker<Iterable<MatchUser>, Void> {

		private String username;

		public MatchmakerWorker(String username) {
			this.username = username;
		}

		@Override
		protected Iterable<MatchUser> doInBackground() throws Exception {
			MatchmakerSystem ms = new MatchmakerSystem();
			return ms.searchMatches(this.username, 0.0);
		}

		@Override
		protected void done() {
			try {
				ArrayList<Object[]> dataBuffer = new ArrayList<>();

				String score_format = "%.4f";
				Iterable<MatchUser> matches = this.get();
				for (MatchUser match : matches) {
					Object[] datum = { match.getUser().getUsername(),
							String.format(score_format, match.getTaxaMatchmaker()) };
					dataBuffer.add(datum);
					// System.out.println(match.getUser().getUsername() + " " +
					// String.format(score_format, match.getTaxaMatchmaker()));
				}

				Object[][] data = new Object[dataBuffer.size()][];
				data = dataBuffer.toArray(data);

				MainWindow.this.scoreTable = new JTable(data, MainWindow.this.scoreTableColumns);
				MainWindow.this.resultPanel.add(MainWindow.this.scoreTable.getTableHeader(), BorderLayout.PAGE_START);
				MainWindow.this.resultPanel.add(MainWindow.this.scoreTable, BorderLayout.CENTER);
				MainWindow.this.resultPanel.revalidate();

			} catch (ExecutionException e) {
				Throwable error = e.getCause();
				if (error == null) {
					JOptionPane.showMessageDialog(null, "Unkown error: " + e.getMessage());
				} else if (error instanceof UserNotFoundException) {
					JOptionPane.showMessageDialog(null, "User not found.");
				} else if (error instanceof UserTextNotFoundException) {
					JOptionPane.showMessageDialog(null, "Text not found for user.");
				} else if (error instanceof TwitterConnectionException) {
					JOptionPane.showMessageDialog(null, "Connection Error.");
				}
				MainWindow.this.clearScoreTable();

			} catch (InterruptedException e) {
				JOptionPane.showMessageDialog(null, "Unkown error.");
			}
		}

	}

	private JFrame frame;
	private JTextField usernameTextField;
	private JTable scoreTable;

	private JPanel resultPanel;

	private final String[] scoreTableColumns = { "User", "Match Score" };

	/**
	 * Create the application.
	 */
	public MainWindow() {
		this.initialize();
	}

	private void clearScoreTable() {
		this.resultPanel.removeAll();
		this.resultPanel.revalidate();
		this.resultPanel.repaint();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setBounds(100, 100, 300, 300);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setTitle(FrameworkSettings.getGlobalSettings().getAppName());

		JPanel searchPanel = new JPanel();
		this.frame.getContentPane().add(searchPanel, BorderLayout.NORTH);

		this.usernameTextField = new JTextField();
		searchPanel.add(this.usernameTextField);
		this.usernameTextField.setColumns(15);

		JButton searchButton = new JButton("Search");
		searchButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new MatchmakerWorker(MainWindow.this.usernameTextField.getText()).execute();
			}
		});
		searchPanel.add(searchButton);

		this.resultPanel = new JPanel();
		this.resultPanel.setLayout(new BorderLayout());

		this.frame.getContentPane().add(this.resultPanel, BorderLayout.CENTER);
	}

	@Override
	public void run() {
		this.frame.setVisible(true);
	}
}
