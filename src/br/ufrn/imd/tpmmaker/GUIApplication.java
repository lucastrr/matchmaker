/**
 ===============================================================================
 FILE...............: GUIApplication.java
 COMMENTS...........: Code responsible for GUI application.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker;

import java.awt.EventQueue;

import br.ufrn.imd.tpmmaker.gui.MainWindow;

/**
 * Visual interface from the application.
 */
public class GUIApplication {

	public static void run(FrameworkSettings settings) {
		FrameworkSettings.setGlobalSettings(settings);
		EventQueue.invokeLater(() -> new MainWindow().run());
	}
}
