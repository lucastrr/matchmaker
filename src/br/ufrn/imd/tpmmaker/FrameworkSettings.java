package br.ufrn.imd.tpmmaker;

import br.ufrn.imd.tpmmaker.matching.Filter;
import br.ufrn.imd.tpmmaker.matching.PatternComparator;
import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.UpdatePolicy;

public class FrameworkSettings {

	private static FrameworkSettings globalSettings = null;

	public static synchronized void setGlobalSettings(FrameworkSettings settings) {
		globalSettings = settings;
	}

	public static synchronized FrameworkSettings getGlobalSettings() {
		return globalSettings;
	}

	private String app_name;

	private DataMiner dataminer;

	private Parser parser;

	private PatternComparator comparator;

	private Filter filter;

	private UpdatePolicy update_policy;

	public FrameworkSettings(String name, DataMiner dataminer, Parser parser, PatternComparator comparator,
			Filter filter, UpdatePolicy update_policy) {
		this.app_name = name;
		this.parser = parser;
		this.dataminer = dataminer;
		this.comparator = comparator;
		this.filter = filter;
		this.update_policy = update_policy;
	}

	public DataMiner getDataMiner() {
		return dataminer;
	}

	public Parser getParser() {
		return parser;
	}

	public PatternComparator getComparator() {
		return comparator;
	}

	public String getAppName() {
		return app_name;
	}

	public String getDBName() {
		return app_name.replace(" ", "").toLowerCase();
	}

	public Filter getFilter() {
		return filter;
	}

	public UpdatePolicy getUpdatePolicy() {
		return update_policy;
	}
}
