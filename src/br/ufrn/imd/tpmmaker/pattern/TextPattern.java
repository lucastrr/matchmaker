/**
 ===============================================================================
 FILE...............: TextPattern.java
 COMMENTS...........: Code responsible for implementing and managing all part
 			........: of the application responsible by taking care of the
 			........: text repeat analysis.
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.pattern;

import java.util.HashMap;
import java.util.Set;

public class TextPattern {

	private int total_frequency;
	private Double variation;

	private HashMap<Token, Integer> frequency_map;

	/**
	 * Default creator.
	 */
	public TextPattern() {
		this.total_frequency = 0;
		this.variation = null;
		this.frequency_map = new HashMap<>();
	}

	public double getFrequencyAverage() {
		return this.getTotalFrequency() / (double) this.getTokenCount();
	}

	public double getFrequencySD() {
		return Math.sqrt(this.getFrequencyVariation());
	}

	public double getFrequencyVariation() {
		if (this.variation == null) {
			double var = 0;
			double mean = this.getFrequencyAverage();

			for (int f : this.frequency_map.values()) {
				double diff = f - mean;
				var += diff * diff;
			}
			this.variation = new Double(var);
		}
		return this.variation.doubleValue();
	}

	public int getTokenCount() {
		return this.frequency_map.size();
		// >>>>>>> 02095c6e1119d2774c453fb87e026347041b2079
	}

	/**
	 * @description Responsible method for checking how often the token has
	 *              appeared
	 * @param tk
	 *            : Token. It is the token that will be analyzed
	 * @return int : It represents the value of the amount of times the token
	 *         appeared
	 * @exception NullpointerException
	 *                : If the token is not initialized
	 */
	public int getTokenFrequency(Token tk) {
		try {
			return this.frequency_map.get(tk);
		} catch (NullPointerException e) {
			return 0;
		}
	}

	public double getTokenRelativeFrequency(Token tk) {
		return this.getTokenRelativeFrequency(tk) / this.getTotalFrequency();
	}

	public Iterable<Token> getTokens() {
		return this.frequency_map.keySet();
	}

	public Set<Token> getTokenSet() {
		return this.frequency_map.keySet();
	}

	public int getTotalFrequency() {
		return this.total_frequency;
	}

	/**
	 * @description Method responsible for checking if there is a token and
	 *              increase its value
	 * @param tk
	 *            : Token. It is the token that will be analyzed
	 * @return void
	 * @exception NullpointerException
	 *                : If the token is not initialized
	 */
	public void incrementTokenFrequency(Token tk) {
		int value = 1;
		try {
			value = this.frequency_map.get(tk) + 1;
		} catch (NullPointerException e) {
			// ignore
		}
		this.total_frequency += 1;
		this.variation = null;
		this.frequency_map.put(tk, value);
	}

	/**
	 * @description Method responsible for checking if there is a token and
	 *              increase its value
	 * @param tk
	 *            : Token. It is the token that will be analyzed
	 * @param frequency
	 *            : Value to be incremented
	 * @return void
	 * @exception NullpointerException
	 *                : If the token is not initialized
	 */
	public void incrementTokenFrequency(Token tk, int frequency) {
		int value = frequency;
		try {
			value = this.frequency_map.get(tk) + frequency;
		} catch (NullPointerException e) {
			// ignore
		}
		this.total_frequency += frequency;
		this.variation = null;
		this.frequency_map.put(tk, value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("TextPattern {");
		for (Token tk : this.getTokens()) {
			Integer value = this.frequency_map.get(tk);
			String desc = "(" + tk.toString() + ": " + value.toString() + ") \n";
			sb.append(desc);
		}
		sb.append("}");
		return sb.toString();
	}
}