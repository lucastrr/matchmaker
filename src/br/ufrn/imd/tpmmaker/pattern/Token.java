/**
 ===============================================================================
 FILE...............: Token.java
 COMMENTS...........: Interface responsible for being the token (hashtags
 			........: and words).
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.pattern;

/**
 * Text entity unit
 */
public abstract class Token implements Comparable<Token> {

	@Override
	public int compareTo(Token that) {
		int hash1 = this.getHash();
		int hash2 = that.getHash();
		if (hash1 < hash2) {
			return -1;
		}
		if (hash1 == hash2) {
			return 0;
		}
		return 1;
	}

	@Override
	public boolean equals(Object that) {
		if (!(that instanceof Token)) {
			return false;
		}
		return (this.getHash() == ((Token) that).getHash());
	}

	/**
	 * @return hashcode identifing the entity
	 */
	public abstract int getHash();

	@Override
	public int hashCode() {
		return this.getHash();
	}
}