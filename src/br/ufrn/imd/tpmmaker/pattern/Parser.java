package br.ufrn.imd.tpmmaker.pattern;

import java.util.Collection;

import br.ufrn.imd.tpmmaker.persistence.UserText;

public interface Parser {
	public TextPattern BuildTextPattern(Collection<UserText> text);
}
