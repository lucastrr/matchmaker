/**
 ===============================================================================
 FILE...............: ReferenceToken.java
 COMMENTS...........: .
 ===============================================================================
 AUTHOR.............: Lucas Torres
 					  Samuel Nata
                      Vitor Godeiro
                      Discipline: Projeto de Software - DIM0600
                      Instituto Metropole Digital
                      Universidade Federal do Rio Grande do Norte
                      Natal, Rio Grande do Norte, Brasil
 LAST MODIFIED......: 29 de September de 2016
 ===============================================================================
*/

package br.ufrn.imd.tpmmaker.pattern;

public class ReferenceToken extends Token {

	int hashcode;

	public ReferenceToken(int hashcode) {
		this.hashcode = hashcode;
	}

	@Override
	public int getHash() {
		return this.hashcode;
	}

}
