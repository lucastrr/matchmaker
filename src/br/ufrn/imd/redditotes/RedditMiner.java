package br.ufrn.imd.redditotes;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import br.ufrn.imd.tpmmaker.User;
import br.ufrn.imd.tpmmaker.persistence.DataMiner;
import br.ufrn.imd.tpmmaker.persistence.UserNotFoundException;
import br.ufrn.imd.tpmmaker.persistence.UserText;
import br.ufrn.imd.tpmmaker.persistence.UserTextNotFoundException;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.JSONTokener;

public class RedditMiner implements DataMiner {

	@Override
	public ArrayList<UserText> fetchTextData(User user) throws UserTextNotFoundException {

		ArrayList<UserText> texts = new ArrayList<UserText>();

		URL url;

		try {
			url = new URL("https://www.reddit.com/r/" + user.getUsername() + "/.json");
			// url = new URL("http://localhost:8000/" + user.getUsername() +
			// ".json");
			URLConnection conn = url.openConnection();

			System.err.println("url: " + url.toString());

			JSONTokener tokener = new JSONTokener(conn.getInputStream());
			JSONObject json = new JSONObject(tokener);

			JSONArray posts = json.getJSONObject("data").getJSONArray("children");
			for (int i = 0; i < posts.length(); i++) {
				String headline = posts.getJSONObject(i).getJSONObject("data").getString("title");
				// System.err.println("headline \"" + headline + "\"");
				RedditText rt = new RedditText(headline);
				texts.add(rt);
			}

		} catch (IOException | JSONException e) {
			e.printStackTrace();
			throw new UserTextNotFoundException();
		}

		return texts;
	}

	@Override
	public User getUserByUsername(String username) throws UserNotFoundException {
		return new User(username.hashCode(), username);
	}

}
