package br.ufrn.imd.redditotes;

import br.ufrn.imd.tpmmaker.pattern.Token;

public class Word extends Token {

	private String word;

	public Word(String word) {
		this.word = word.toLowerCase();
	}

	@Override
	public int getHash() {
		return 243 + word.hashCode();
	}

}
