package br.ufrn.imd.redditotes;

import java.util.Collection;

import br.ufrn.imd.tpmmaker.pattern.Parser;
import br.ufrn.imd.tpmmaker.pattern.TextPattern;
import br.ufrn.imd.tpmmaker.pattern.Token;
import br.ufrn.imd.tpmmaker.persistence.UserText;

public class RedditParser implements Parser {

	@Override
	public TextPattern BuildTextPattern(Collection<UserText> text) {
		TextPattern tp = new TextPattern();

		for (UserText st : text) {
			String raw_text = st.getText();

			String[] entities = raw_text.split("\\W+");
			for (String entity : entities) {

				if (entity.isEmpty()) {
					continue;
				} else {
					Token token = new Word(entity);
					tp.incrementTokenFrequency(token);
				}
			}
		}

		return tp;
	}
}
