package br.ufrn.imd.redditotes;

import br.ufrn.imd.tpmmaker.persistence.UserText;

public class RedditText implements UserText {

	public String text;

	public RedditText(String text) {
		this.text = text;
	}

	@Override
	public String getLang() {
		return "en";
	}

	@Override
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
